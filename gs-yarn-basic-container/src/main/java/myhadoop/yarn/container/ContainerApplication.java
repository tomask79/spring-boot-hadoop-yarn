package myhadoop.yarn.container;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import myhadoop.yarn.container.custom.*;

@Configuration
@EnableAutoConfiguration
public class ContainerApplication {
				
	@Bean(name="yarnContainerRef")
	public MyContainerImplementation getYarnContainerRef() {
		return new MyContainerImplementation();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ContainerApplication.class, args);
	}
}