package myhadoop.yarn.container.custom;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.yarn.container.DefaultYarnContainer;
import org.springframework.yarn.listener.ContainerStateListener;

public class MyContainerImplementation extends DefaultYarnContainer {
	private static final Log log = LogFactory.getLog(MyContainerImplementation.class);
				
	public MyContainerImplementation() {
		super();
		
		log.info("...Initializing yarn MyContainerImplementation....");
				
		
		this.addContainerStateListener(new ContainerStateListener() {

			@Override
			public void state(ContainerState state, Object exit) {
				switch(state) {
					case COMPLETED: {
						log.info("...Container started successfully!...");
						// YOUR TASK CODE GOES HERE.... 
						break;
					}
					case FAILED: {
						log.info("...Starting of container failed!...");
						break;
					}
					default: {
						log.info("Unexpected container state...exiting!...");
					}
				}
			}
		});
	}
	
	public void runInternal() {
		log.info("...Running internal method...");
	}
}
