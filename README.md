# Running non-mapreduce job at hadoop 2 cluster with SpringBoot and SpringYarn integration #

In the past days I wanted to test one of the main benefits of using Hadoop 2 generation platform 
which is *possibility of running non-mapreduce* job inside of Hadoop. Before we will get to business, let's repeat the basics of difference between Hadoop first and second generation...

Hadoop 1 cons:

- Only MapReduce jobs are possible to run.
- NamedNode is a single point of failure.
- JobTracker is highly overloaded and also single point of failure.

Hadoop 2 benefits:

- Thanks to the new Yarn system of components, we can run non-mapreduce tasks inside of Hadoop.
- Own task is running inside of Yarn component called YarnContainer which resource requirements are monitored via new Yarn component called YarnAppMaster by communicating with ResourceManager. So we don't need a single JobTracker to launch and monitor all of the Hadoop tasks. All Yarn applications have it's own YarnAppMaster.


## My SpringYarn demo ##

As a framework I have chosen SpringYarn integration from Spring-Data Hadoop project.
If you want to create new SpringYarn tasks, do not complicate things to yourself and simply
git clone following repo:

```
git clone https://github.com/spring-guides/gs-yarn-basic.git

```
and jump into **initial** directory. Here you've got complete template of three basic Yarn components to create Yarn application. YarnClient, YarnContainer and YarnAppMaster. Don't forget, component where your non-mapreduce task is running is **YarnContainer**.

You can pretty much follow tutorial here:

https://spring.io/guides/gs/yarn-basic/

Be careful with paths in yml files. I'm launching SpringYarn client from ..gs-yarn-basic/initial/gs-yarn-basic-dist/ directory so I've got following paths to jar files in yml:


```
file:target/gs-yarn-basic-dist/gs-yarn-basic-container-0.1.0.jar
file:target/gs-yarn-basic-dist/gs-yarn-basic-appmaster-0.1.0.jar

```

paths in yml have to reflect place from where you launch YarnClient. Also, when
creating yml files, don't forget to set URL to your Hadoop 2 installation.

### Goal of my SpringYarn demo ###

Well, I didn't want to just follow mentioned tutorial, I wanted to customize YarnContainer by myself just for test. This isn't easy and obvious at all. I needed help with that from author of SpringYarn integration Janne Valkealahti, read here:

http://stackoverflow.com/questions/32129140/customizing-yarn-container/32171314#32171314

Well, if you configured in my demo here everything correctly then you should see in the Hadoop logs:



```
.   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.2.3.RELEASE)

[2015-08-27 00:16:04.129] boot - 2315  INFO [main] --- ContainerApplication: Starting ContainerApplication v0.1.0 on tomask79.local with PID 2315 (/usr/local/Cellar/hadoop/hdfs/tmp/nm-local-dir/usercache/tomask79/appcache/application_1440621341119_0011/filecache/11/gs-yarn-basic-container-0.1.0.jar started by tomask79 in /usr/local/Cellar/hadoop/hdfs/tmp/nm-local-dir/usercache/tomask79/appcache/application_1440621341119_0011/container_1440621341119_0011_01_000002)
[2015-08-27 00:16:04.212] boot - 2315  INFO [main] --- AnnotationConfigApplicationContext: Refreshing org.springframework.context.annotation.AnnotationConfigApplicationContext@7803e4a4: startup date [Thu Aug 27 00:16:04 CEST 2015]; root of context hierarchy
[2015-08-27 00:16:07.932] boot - 2315  INFO [main] --- AutowiredAnnotationBeanPostProcessor: JSR-330 'javax.inject.Inject' annotation found and supported for autowiring
[2015-08-27 00:16:08.389] boot - 2315  INFO [main] --- MyContainerImplementation: ...Initializing yarn MyContainerImplementation....
[2015-08-27 00:16:08.825] boot - 2315  INFO [main] --- ThreadPoolTaskExecutor: Initializing ExecutorService  'metricsExecutor'
[2015-08-27 00:16:10.028] boot - 2315  INFO [main] --- AnnotationMBeanExporter: Registering beans for JMX exposure on startup
[2015-08-27 00:16:10.052] boot - 2315  INFO [main] --- DefaultLifecycleProcessor: Starting beans in phase 0
[2015-08-27 00:16:10.053] boot - 2315  INFO [main] --- LifecycleObjectSupport: started myhadoop.yarn.container.custom.MyContainerImplementation@220ca8ce
[2015-08-27 00:16:10.562] boot - 2315  INFO [main] --- ContainerApplication: Started ContainerApplication in 7.266 seconds (JVM running for 8.308)
[2015-08-27 00:16:10.570] boot - 2315  INFO [Thread-2] --- AnnotationConfigApplicationContext: Closing org.springframework.context.annotation.AnnotationConfigApplicationContext@7803e4a4: startup date [Thu Aug 27 00:16:04 CEST 2015]; root of context hierarchy
[2015-08-27 00:16:10.575] boot - 2315  INFO [Thread-2] --- DefaultYarnContainer: Setting contextClosing flag because of ContextClosedEvent
[2015-08-27 00:16:10.577] boot - 2315  INFO [Thread-2] --- DefaultLifecycleProcessor: Stopping beans in phase 0
[2015-08-27 00:16:10.579] boot - 2315  INFO [Thread-2] --- DefaultYarnContainer: Stopping DefaultYarnContainer and cancelling Futures
[2015-08-27 00:16:10.579] boot - 2315  INFO [Thread-2] --- DefaultYarnContainer: About to notifyEndState from doStop because contextClosing=true
[2015-08-27 00:16:10.579] boot - 2315  INFO [Thread-2] --- DefaultYarnContainer: Container state based on method results=[] runtimeException=[null]
[2015-08-27 00:16:10.580] boot - 2315  INFO [Thread-2] --- AbstractYarnContainer: Notifying listeners of ContainerState=COMPLETED and exit=0
[2015-08-27 00:16:10.580] boot - 2315  INFO [Thread-2] --- MyContainerImplementation: ...Container started successfully!...
[2015-08-27 00:16:10.580] boot - 2315  INFO [Thread-2] --- LifecycleObjectSupport: stopped myhadoop.yarn.container.custom.MyContainerImplementation@220ca8ce
[2015-08-27 00:16:10.582] boot - 2315  INFO [Thread-2] --- AnnotationMBeanExporter: Unregistering JMX-exposed beans on shutdown
[2015-08-27 00:16:10.584] boot - 2315  INFO [Thread-2] --- ThreadPoolTaskExecutor: Shutting down ExecutorService 'metricsExecutor'
~                                                                                                                                                                                    
~                                                                                                                                                                                    
~                                                                                                                                                                                    
~               
```
Congratulations! You've build simple Spring Yarn application with customized Yarn Container. Notice how our MyContainerImplementation YarnContainer was started and stopped and how we caught start of container in Yarn ContainerStateListener...Adding of your application code into it is just formality.


### Final result of test:###

From my point of view, SpringYarn integration definitely needs two things:

* Far better documentation! A lot of things need to be figured out by reading the source code..:(
* And most of all...time! For example ApacheTwill looks much better option to me now. Because it focuses me on my task only...I didn't understand for example, why am I forced to write App master application in Spring Yarn...:(


Well, good luck.